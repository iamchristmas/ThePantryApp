DROP DATABASE IF EXISTS pantrypal;
CREATE DATABASE pantrypal;
USE pantrypal;
GO
DROP TABLE IF EXISTS families;
CREATE TABLE families
    (
        familyID	int IDENTITY(1,1)	PRIMARY KEY,
        famaddress  NVARCHAR(30)        NOT NULL, --gotta secure this in the future.
        city        NVARCHAR(30)        NOT NULL, --gotta secure this in the future.
        country     NVARCHAR(30)        NOT NULL, --gotta secure this in the future.
        postalcode  NVARCHAR(10)        NOT NULL, --gotta secure this in the future.
        phone       NVARCHAR(24)        NOT NULL --gotta secure this in the future.
    );
INSERT INTO families (famaddress,city,country,postalcode,phone)
values('123 Sesame street','New York City','USA','12345','555-555-5555');

DROP TABLE IF EXISTS users;
CREATE TABLE users
    (
        userID UNIQUEIDENTIFIER DEFAULT NEWSEQUENTIALID() PRIMARY KEY ,
        email       NVARCHAR(50)        NOT NULL,
        password    NVARCHAR(30)        NOT NULL, --gotta secure this in the future.
        firstname   NVARCHAR(255)       NOT NULL,
        middlename  NVARCHAR(255)       ,
        lastname    NVARCHAR(255)       NOT NULL,
        birthday    DATE                NOT NULL, --gotta secure this in the future.
		familyID INT NOT NULL,
		CONSTRAINT FK_familyID
			FOREIGN KEY (familyID)
			REFERENCES families (familyID)
    );

INSERT INTO users (email,password,firstname,middlename,lastname,birthday,familyID)
values('chris.smith@gmail.com','password','Chris','B','Smith','19940926',1);

DROP TABLE IF EXISTS diets;
CREATE TABLE diets
    (
        dietID		int					IDENTITY(0,10) PRIMARY KEY,
        dietname	NVARCHAR(30)		NOT NULL,
    );

DROP TABLE IF EXISTS recipe;
CREATE TABLE recipe
	(
		RecipeID NVARCHAR(24)			PRIMARY KEY,-- Reflects ObjectID in mongo instance
		RecipeName NVARCHAR(255)			
	);

DROP TABLE IF EXISTS products;
CREATE TABLE products
	(
		Productid	int IDENTITY(1,1)	PRIMARY KEY
		qty			int,				
		sale		bit,				
		SKU			NVARCHAR(25)		
		UPC			NVARCHAR(18)		
	);