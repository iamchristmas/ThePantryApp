# Testing
Return to main main project page [here](https://github.com/iamchristmas/ThePantryApp)
## Functional Requirements
|No.  |Component  |Test  | Type | Status | Timestamp | Req | Release |    
|-----|-----|-----|-----|-----|-----|-----|-----|
|1.0|cupboard_login_integration_test| User is able to log-in to web application using a valid email and up to 256-character password containing any valid ascii character (spaces not included) and falls back to a signup page if no valid account is found. |Test |Untested|20200308 0000PST|1.0|0.0|
|1.0.1|cupboard_inputvalidation_unit_test| Application checks that email is formatted as *@*.com . |Test |Untested|20200308 0000PST|1.0|0.0|
|1.0.2|cupboard_authorization_unit_test| Application checks UserDB for existing account and returns a token valid for 12-hours. |Test |Untested|20200308 0000PST|1.0|0.0|
|1.0.3|cupboard_fallback_unit_test| Application provides user with sign-up prompt after failed login. |Test |Untested|20200308 0000PST|1.0|0.0|
|1.0.4|cupboard_oauth_integration_test| Application implements external validation using OAuth. |Test |Untested|20200308 0000PST|1.2|0.0|
|1.0.4.1|cupboard_fblogin_unit_test| Application implements external validation Facebook identity providers. |Test |Untested|1.2.1|0.0|
|1.0.4.2|cupboard_msftlogin_unit_test| Application implements external validation Microsoft identity providers. |Test |Untested|1.2.2|0.0|
|1.0.4.3|cupboard_glogin_unit_test| Application implements external validation Google identity providers. |Test |Untested|1.2.3|0.0|
|1.0.4.4|cupboard_fbsignup_unit_test| Application implements fallback sign up using Facebook identity providers. |Test |Untested|20200308 0000PST|1.0|0.0|
|1.0.4.5|cupboard_msftsignup_unit_test| Application implements fallback sign up using Microsoft identity providers. |Test |Untested|20200308 0000PST|1.0|0.0|
|1.0.4.6|cupboard_oauth_gsignup_test| Application implements fallback sign up using Google identity providers. |Test |Untested|20200308 0000PST|1.0|0.0|
|1.1|cupboard_homepage_integration_test|Upon login, user is displayed home page containing welcome message, recommended recipes, and ingredients close to expiration on a main window.| Inspection |Untested|20200308 0000PST||0.0|
|1.2|cupboard_navbar_integration_test|Upon login, user is displayed navigation bar on the left-side of the page with links to the home page, grocery inventory, recipe list, shopping list, and settings.| Inspection |Untested|20200308 0000PST||0.0|
|1.2.1|cupboard_navbarhome_unit_test|When clicked, the home button sends user to the home page|Test |Untested|20200308 0000PST|1.0|0.0|
|1.2.2|cupboard_navbargroceries_unit_test|When clicked, the home button sends user to the grocery page|Test |Untested|20200308 0000PST|1.0|0.0|
|1.2.3|cupboard_navbarrecipe_unit_test|When clicked, the home button sends user to the recipe page|Test |Untested|20200308 0000PST|1.0|0.0|
|1.2.4|cupboard_navbarlist_unit_test|When clicked, the home button sends user to the shopping list page|Test |Untested|20200308 0000PST|1.0|0.0|
|1.2.5|cupboard_navbarsetting_unit_test|When clicked, the home button sends user to the setting page|Test |Untested|20200308 0000PST|1.0|0.0|
|1.3|cupboard_ingredientspage_unit_test|Application implements grocery page that contains a list of all ingredients on hand, their quantities, nutritional values, and an interface allowing for the manipulation of the list.|Inspection |Untested|20200308 0000PST|1.0|0.0|
|1.3.1|cupboard_ingredientadd_unit_test|Grocery page view allows users to add new groceries.|Test |Untested|20200308 0000PST|1.0|0.0|
|1.3.2|cupboard_ingredientdelete_unit_test|Grocery page view allows users to delete groceries.|Test |Untested|20200308 0000PST|1.0|0.0|
|1.3.3|cupboard_ingredientmodify_unit_test|Grocery page view allows users to modify quantities of groceries.|Test |Untested|20200308 0000PST|1.0|0.0|
|1.4|cupboard_settings_unit_test|Application implements settings page that allows user to modify food preferences and family settings.|Inspection |Untested|20200308 0000PST|1.0|0.0|
|2.0|recipebook_page_integration_test|Application implements recipe page that contains a list of all favorited recipes and recommended recipes.|Inspection |Untested|20200308 0000PST|1.0|0.0|
|2.0.1|recipebook_page_integration_test|Each entry displays the name of the dish, the cuisine style, dietary constraints, prep time, serving size, and an indicator of whether they have the requisite ingredients.|Inspection |Untested|20200308 0000PST|1.0|0.0|
|2.0.1.1|recipebook_details_unit_test|Upon clicking a recipe, a new window appears displaying the full recipe containing ingredients and instructions.| Test |Untested|20200308 0000PST|1.0|0.0|
|2.0.1.2|recipebook_addtolist_unit_test|Upon clicking add to cart button, application will add deficient items to shopping list.|Test |Untested|20200308 0000PST|1.0|0.0|
|2.0.1.3|recipebook_favorite_unit_test|Upon clicking a star on the recipe page, the recipe will be added to the user's recipe book.| Test |Untested|20200308 0000PST|1.0|0.0|
|4.0|collander_pull_integration_test|Application fetches recipes from Allrecipes.com in a JSON format then uploads them to MongoDB for archiving.|Test |Untested|20200308 0000PST|1.0|0.0|
|4.0.1|collander_normalize_unit_test|Application normalizes all recipe measurements before uploading.|Test |Untested|20200308 0000PST|1.0|0.0|
|4.0.1.1|collander_volliquid_unit_test|Application converts all volumetric fluid measurements to millileters.|Test |Untested|20200308 0000PST|1.0|0.0|
|4.0.1.2|collander_mass_unit_test|Application converts all mass measurements to grams.|Test |Untested|20200308 0000PST|1.0|0.0|
|4.0.1.3|collander_volsolid_unit_test|Application converts all volumetric solid measurements to millileters.|Test |Untested|20200308 0000PST|1.0|0.0|
|5.0|GroceryList|Application implements shopping list page that contains a running list of all ingredients a user has added in a session.|Test |Untested|20200308 0000PST|1.0|0.0|
|xxx|Grocer|Pending implementation|Test |Untested|20200308 0000PST|1.0|0.0|
|xxx|Errand|Pending implementation|Test |Untested|20200308 0000PST|1.0|0.0|
|xxx|Palate|Pending implementation|Test |Untested|20200308 0000PST|1.0|0.0|
