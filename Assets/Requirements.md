# Requirements
Return to main main project page [here](https://github.com/iamchristmas/ThePantryApp)
## Scope
* Create user accounts or associate with external identity provider.
* Associate users within a household.
* Manage all ingredients within a household.
* Manage recipes cooked, desired, or favorited in a household.
* Generate shopping lists.
* Enable user's to order their shopping list from their preffered grocer.

## Definitions, Acronyms, and Abbreviations
* SQL - Structured Query Language
* DB - Database 
* SPWA - Single-Paged Web App
* UPC - Universal Product Code
* SKU - Stock Keeping Unit
* API - Application Programming Interface
* JS - Javascript
* HTML - Hypertext Markup Language
* JSON - Javascript Object Notation
* XML - eXtensible Hypertext Markup Languge
* REST - Representational State Transfer
* K8S - Kubernetes
* SSL - Secure Sockets Layer

## Technologies to be used
* Azure CosmoDB
* Azure SQL
* Azure Container Services
* K8S
* Languages
    * C#
    * Python
    * JS
* Visual Studio
* Microservice Architecture

## Purpose
To develop an interactive single-page web app that can enable a user to inventory their groceries and generate a list of recommended recipes then provide a list of any deficient ingredients and order them from an external business.

### Users
Homecook
* Upon implementation of the system, users shall find site navigation, grocery management, recipe management, and shopping lists. Consumers shall be able to use the groceries on-hand to generate a list of ingredients and order any which they are deficient on.
Product Vendors
* The system shall allow for vendors to integrate their online stores to allow users to easily order products for pickup or delivery

### Location 
The System shall reside in the Azure Cloud and shall be accesible from anywhere in the world.

## User stories

1. As a homecook, I need to be able to access a list of my ingredients so that I can know what I have on hand.
2. As a homecook, I need to view recipes so that I can identify meals that I want to cook.
3. As a homecook, I need to be able to see deficient ingredients so that I can know what I need to buy from the grocery store.
4. As a homecook, I want to be able to order the items I need for a recipe from my favorite grocery store.

## Use-Cases
1. Given a log-in page, the actor shall be able to log in to the system using a username and password or connected authentication service when they provide valid credentials and then view their recipe dashboard.
2. Given a a view of an ingredient inventory, the actor shall be able to view a list of ingredients that they have on hand when logged in to their profile and then manipulate the list.
3. Given a view of a recipe, the actor shall be able to view the ingredients, instructions, time to cook, and required tools for a recipe and then cross-reference the required items with those that they have on hand.
4. Given a list of deficient goods, the actor shall be able to use their favorite delivery service or grocery store to order the goods and then order the goods with the same interface.



## Functional Requirements
|No.  |Req  |Sat  |  
|-----|-----|-----|
|1.0| The system shall allow a user to login to the web interface. | |
|1.1   | The system shall require a username and password.  |   |
|1.1.1 | The system shall store username and password in an encrypted format. | |
|1.2 | The system shall support OAuth authentication mechanisms. | |
|1.2.1| The system shall allow users to login with google credentials.|| 
|1.2.2| The system shall allow users to login with facebook credentials.|| 
|1.2.3| The system shall allow users to login with microsoft credentials.|| 
|1.3|The system shall produce generic results upon failed login attempts.||
|1.4|The system shall allow for a user to create an account if credentials provided do not match an active user account.||
|2.0|The system shall allow users to create unique profiles.||
|2.1|The system shall allow users to provide an email for mailing lists.||
|2.2|The system shall allow users to provide dietary preferences only viewable by them.||
|2.2|The system shall allow users to associate their account with other accounts.||
|2.2.1|The system shall allow users create logical groupings of accounts.||
|3.0|The system shall track food objects in the possession of a user.||
|3.1|The system shall track the food's nutritional information.||
|3.2|The system shall maintain a generic image of the food.||
|3.3|The system shall allow users to freely add and remove goods.||
|3.3.1|The system shall implement a universal API for adding and removing goods.||
|4.0|The system shall maintain a list of recipes for that the user can make with the food in their inventory.||
|4.1|The system shall organize recipes by cuisine and diet.||
|4.2|The system shall track the ingredients for each recipe.||
|4.3|The system shall be able to recognize any deficient recipe items and generate a list of the required ingredients.||
|4.3.1|The system shall generate a generic datatype for exporting a list of deficient ingredients.||
|5.0|The system shall be capable of interacting with vendors.||
|5.1|The system shall maintain SKUs and UPCs for individual ingredients.||


