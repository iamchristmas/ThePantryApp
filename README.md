# ThePantryApp
This application is designed with the lazy, introverted grocery shopper in mind as a means of easily identifying available recipes and needed groceries. 

Buying groceries is an arduous task. People must decide what they’re cooking, look up the required ingredients, see if they have the ingredients, make trips to the grocery store, and cook the food. Some of these people also hate going grocery shopping due, in part, to either having to get dressed or go outside. If an application were to exist that could not only tell them a recipe to try but do so in the context of what’s in their cupboard, then the need to grocery shop to cook is nullified! The intent behind Pantry Pal is to develop an application that aggregates a user’s grocery inventory, a rough sampling of their preferred taste in food, and provide a list of recommended recipes for them to try within the context of their ingredients on hand. In addition to tracking this information and generating suggestions off their inventory, the application could also serve as a means of generating a shopping list and even go so far as to order products for the user.

## Table of Contents

 1. [Requirements](/Assets/Requirements.md)
 2. [Test Plan](/Assets/Tests.md)
 3. [Components](https://github.com/iamchristmas/ThePantryApp#components)
 4. [UI Design](https://github.com/iamchristmas/ThePantryApp#user-interface-design)
 5. [Future Implementations](https://github.com/iamchristmas/ThePantryApp#future-implementations)

## Components 

ThePantryApp will consist of several components, adopting a microservice architecture:
1.  Inventory Maintainer (Cupboard) - Cupboard will be the front-end application that facilitates user's interacting with their grocery inventory, saved recipes, and orders. Cupboard will reside in a Azure-hosted kubernetes cluster. Using HTML5, CSS and Javascript, this application will require user's to login in to the web interface, or future mobile application interface, using authentication API's provided by Microsoft, Google and Facebook identity management solutions. Once logged-in, the user will be presented with an interface where they can modify their current grocery inventory by adding or deleting items, browse recipes within the RecipeBook based upon what items they have on hand, and interact with the ordering API's of their preferred grocer. 

2.  Database (RecipeBook) - RecipeBook will reside at the heart of the application and interact with various APIs internal to the application. RecipeBook will reside on an Azure SQL instance with CosmosDB instance being a potential candidate for hosting recipes given the tentatively unknown nature of the number of ingredients required. The ingredients list will be populated from datasets that are openly available on the internet. A visualization of the ERD follows:
![Database ERD](/Assets/PantryPalERD.png)

3.  Recommendation Engine (Palate) - The recommendation engine will run queries against a user's known preferences, available ingredient, and various other factors that would affect what they eat and how soon they eat it. This information would also be aggregated and compared against recipes cooked by all users to establish trending recipes.

4.  UPC lookup handler (GroceryList) - The GroceryList will serve as a middle-man for the Grocer, RecipeBook, and storefront API's. GroceryList will ingest inputs received by the UPC scanner application on a mobile phone and query the user's preferred grocer's api for pricing, availability, and other information. This information will then be sanitized and cached in the RecipeBook with a 24-Hour TTL. 

5.  UPC Scanner (Grocer) - Smartphone application for scanning barcodes using google firebase API.

6.  Order utlity (Errand) - Errand will interact with various storefronts to order deficient products for recipes. The primary focus of this project will be to use Wal-Mart's Affiliate API given that it will also be leveraged for pulling pricing data.  

## User Interface Design
The web-interface of the Pantry App will leverage Angular to produce an intuitive and streamlined interface for interacting with the various backend services.

The Home page will show a splash page with a trending list of recipes. From this page, the user will only be able to login to the application.
![Home](/Assets/Home.png)

The login page is self-explanatory. All of the login buttons will be replaced by the respective services logos for ease of communication.
![Login](/Assets/Login.png)

Upon Log-in, the user will immediately be presented a list of ready to cook recipes. With a nav-bar located on the left hand side of the application, the user can immediately access all functions with one click. The entirety of this application will be a single-paged application.  
![User Home](/Assets/UserHome.png)

After clicking the inventory icon, the user will see their current inventory of items on hand and can modify them as necessary either in single or bulk operations.
![Inventory](/Assets/Inventory.png)

An alternative view for the user is a list of recipes where they can find new things to cook and populate a shopping list based on the ingredients of the recipe.
![Recipes](/Assets/Recipes.png)

The items added to the shopping list will be viewable on the shopping list page where user's can add or subtract quantities of items and then send the shopping list to an external service provider for purchasing or delivery.
![Shopping List](/Assets/List.png)



## Future Implementations

* Leverage smartcams to use machine learning to identify ingredients in a fridge and update the inventory based upon information provided by the camera. 
